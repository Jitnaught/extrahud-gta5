﻿using GTA;
using GTA.Native;
using System;
using System.Drawing;

namespace ExtraHUD
{
	public class ExtraHUD : Script
	{
		bool showAmmo, showDate, showDay, showTime, showSpeed, showEngine, showHealth, showArmor, showMoney;
		bool systemTime, militaryTime, kmph;

		UIText dateText = new UIText("", new Point(205, 572), 0.45f), //original y: 592
			dayText = new UIText("", new Point(205, 592), 0.45f), //610
			timeText = new UIText("", new Point(205, 610), 0.45f), //628
			speedText = new UIText("", new Point(200, 628), 0.45f), //646
			engineHealthText = new UIText("", new Point(205, 646), 0.45f), //664
			healthText = new UIText("", new Point(205, 664), 0.45f), //682
			armorText = new UIText("", new Point(205, 682), 0.45f), //700
			moneyText = new UIText("", new Point(205, 700), 0.45f);

		int nextUpdateText = 0;

		public ExtraHUD()
		{
			GetSettings();

			Tick += ExtraHUD_OnTick;
		}

		private void ExtraHUD_OnTick(object sender, EventArgs e)
		{
			if (Game.IsLoading || Game.IsPaused || Game.IsScreenFadedOut ||
				Game.IsScreenFadingOut || Game.IsScreenFadingIn ||
				Function.Call<bool>(Hash.IS_CUTSCENE_ACTIVE) || Function.Call<bool>(Hash.IS_RADAR_HIDDEN)) return;

			if (Function.Call<bool>(Hash.IS_PLAYER_BEING_ARRESTED, Game.Player, true)) return;

			Ped plrPed = Game.Player.Character;
			if (plrPed == null || !plrPed.Exists() || plrPed.IsDead) return;

			if (Game.GameTime > nextUpdateText)
			{
				UpdateTexts(plrPed);
				nextUpdateText = Game.GameTime + 100;
			}

			DrawTexts();
		}

		private void GetSettings()
		{
			systemTime = Settings.GetValue<bool>("SETTINGS", "SYSTEM_TIME", false);
			showAmmo = Settings.GetValue<bool>("SETTINGS", "SHOW_AMMO", true);

			Point tempPoint = new Point();

			tempPoint.X = Settings.GetValue("DATE_TEXT", "X", 205);
			tempPoint.Y = Settings.GetValue("DATE_TEXT", "Y", 572);
			dateText.Position = tempPoint;
			dateText.Font = Settings.GetValue("DATE_TEXT", "FONT", GTA.Font.Pricedown);
			dateText.Scale = Settings.GetValue("DATE_TEXT", "SCALE", 0.45f);
			dateText.Color = Color.FromName(Settings.GetValue<string>("DATE_TEXT", "COLOR", "White"));
			dateText.Outline = Settings.GetValue("DATE_TEXT", "OUTLINE", true);
			showDate = Settings.GetValue("DATE_TEXT", "ENABLED", true);

			tempPoint.X = Settings.GetValue("DAY_TEXT", "X", 205);
			tempPoint.Y = Settings.GetValue("DAY_TEXT", "Y", 592);
			dayText.Position = tempPoint;
			dayText.Font = Settings.GetValue("DAY_TEXT", "FONT", GTA.Font.Pricedown);
			dayText.Scale = Settings.GetValue("DAY_TEXT", "SCALE", 0.45f);
			dayText.Color = Color.FromName(Settings.GetValue<string>("DAY_TEXT", "COLOR", "White"));
			dayText.Outline = Settings.GetValue("DAY_TEXT", "OUTLINE", true);
			showDay = Settings.GetValue("DAY_TEXT", "ENABLED", true);

			tempPoint.X = Settings.GetValue("TIME_TEXT", "X", 205);
			tempPoint.Y = Settings.GetValue("TIME_TEXT", "Y", 610);
			timeText.Position = tempPoint;
			timeText.Font = Settings.GetValue("TIME_TEXT", "FONT", GTA.Font.Pricedown);
			timeText.Scale = Settings.GetValue("TIME_TEXT", "SCALE", 0.45f);
			timeText.Color = Color.FromName(Settings.GetValue<string>("TIME_TEXT", "COLOR", "White"));
			timeText.Outline = Settings.GetValue("TIME_TEXT", "OUTLINE", true);
			showTime = Settings.GetValue("TIME_TEXT", "ENABLED", true);
			militaryTime = Settings.GetValue("TIME_TEXT", "24_HOUR_TIME", false);

			tempPoint.X = Settings.GetValue("SPEED_TEXT", "X", 205);
			tempPoint.Y = Settings.GetValue("SPEED_TEXT", "Y", 628);
			speedText.Position = tempPoint;
			speedText.Font = Settings.GetValue("SPEED_TEXT", "FONT", GTA.Font.Pricedown);
			speedText.Scale = Settings.GetValue("SPEED_TEXT", "SCALE", 0.45f);
			speedText.Color = Color.FromName(Settings.GetValue<string>("SPEED_TEXT", "COLOR", "White"));
			speedText.Outline = Settings.GetValue("SPEED_TEXT", "OUTLINE", true);
			showSpeed = Settings.GetValue("SPEED_TEXT", "ENABLED", true);
			kmph = Settings.GetValue("SPEED_TEXT", "KMPH", false);

			tempPoint.X = Settings.GetValue("ENGINE_TEXT", "X", 205);
			tempPoint.Y = Settings.GetValue("ENGINE_TEXT", "Y", 646);
			engineHealthText.Position = tempPoint;
			engineHealthText.Font = Settings.GetValue("ENGINE_TEXT", "FONT", GTA.Font.Pricedown);
			engineHealthText.Scale = Settings.GetValue("ENGINE_TEXT", "SCALE", 0.45f);
			engineHealthText.Color = Color.FromName(Settings.GetValue<string>("ENGINE_TEXT", "COLOR", "White"));
			engineHealthText.Outline = Settings.GetValue("ENGINE_TEXT", "OUTLINE", true);
			showEngine = Settings.GetValue("ENGINE_TEXT", "ENABLED", true);

			tempPoint.X = Settings.GetValue("HEALTH_TEXT", "X", 205);
			tempPoint.Y = Settings.GetValue("HEALTH_TEXT", "Y", 664);
			healthText.Position = tempPoint;
			healthText.Font = Settings.GetValue("HEALTH_TEXT", "FONT", GTA.Font.Pricedown);
			healthText.Scale = Settings.GetValue("HEALTH_TEXT", "SCALE", 0.45f);
			healthText.Color = Color.FromName(Settings.GetValue<string>("HEALTH_TEXT", "COLOR", "White"));
			healthText.Outline = Settings.GetValue("HEALTH_TEXT", "OUTLINE", true);
			showHealth = Settings.GetValue("HEALTH_TEXT", "ENABLED", true);

			tempPoint.X = Settings.GetValue("ARMOR_TEXT", "X", 205);
			tempPoint.Y = Settings.GetValue("ARMOR_TEXT", "Y", 682);
			armorText.Position = tempPoint;
			armorText.Font = Settings.GetValue("ARMOR_TEXT", "FONT", GTA.Font.Pricedown);
			armorText.Scale = Settings.GetValue("ARMOR_TEXT", "SCALE", 0.45f);
			armorText.Color = Color.FromName(Settings.GetValue<string>("ARMOR_TEXT", "COLOR", "White"));
			armorText.Outline = Settings.GetValue("ARMOR_TEXT", "OUTLINE", true);
			showArmor = Settings.GetValue("ARMOR_TEXT", "ENABLED", true);

			tempPoint.X = Settings.GetValue("MONEY_TEXT", "X", 205);
			tempPoint.Y = Settings.GetValue("MONEY_TEXT", "Y", 700);
			moneyText.Position = tempPoint;
			moneyText.Font = Settings.GetValue("MONEY_TEXT", "FONT", GTA.Font.Pricedown);
			moneyText.Scale = Settings.GetValue("MONEY_TEXT", "SCALE", 0.45f);
			moneyText.Color = Color.FromName(Settings.GetValue<string>("MONEY_TEXT", "COLOR", "White"));
			moneyText.Outline = Settings.GetValue("MONEY_TEXT", "OUTLINE", true);
			showMoney = Settings.GetValue("MONEY_TEXT", "ENABLED", true);
		}

		private void UpdateTexts(Ped ped)
		{
			if (showHealth) healthText.Caption = $"Health: {ped.Health}";
			if (showArmor) armorText.Caption = $"Armor: {ped.Armor}";
			if (showMoney) moneyText.Caption = $"Money: {Game.Player.Money}";

			if (showDate || showDay || showTime)
			{
				DateTime currentDate;

				if (systemTime) currentDate = DateTime.Now;
				else
				{
					currentDate = World.CurrentDate;
					currentDate = currentDate.AddYears(4).AddDays(-3); // + 4 years to make 2013, -3 days to correct day of week
				}

				if (showDate) dateText.Caption = currentDate.ToString("d");

				if (showDay)
				{
					string day = currentDate.DayOfWeek.ToString();
					day = char.ToUpper(day[0]) + day.Substring(1); //uppercase first letter

					dayText.Caption = day;
				}

				if (showTime)
				{
					if (militaryTime) timeText.Caption = currentDate.ToString("HH:mm");
					else timeText.Caption = currentDate.ToString("hh:mm tt");
				}
			}

			if (showSpeed || showEngine)
			{
				if (ped.IsInVehicle())
				{
					Vehicle veh = ped.CurrentVehicle;

					if (veh != null && veh.Exists())
					{
						if (showEngine)
						{
							engineHealthText.Enabled = true;
							engineHealthText.Caption = $"Engine: {(int)veh.EngineHealth}";
						}

						if (showSpeed)
						{
							speedText.Enabled = true;

							string speed;

							if (kmph) speed = $"{(int)(veh.Speed * 3.6f)} KM/H";
							else speed = $"{(int)(veh.Speed * 2.236936f)} MPH";

							speedText.Caption = $"Speed: {speed}";
						}
					}
				}
				else
				{
					engineHealthText.Enabled = false;
					speedText.Enabled = false;
				}
			}
		}

		private void DrawTexts()
		{
			if (showAmmo) Function.Call(Hash.DISPLAY_AMMO_THIS_FRAME, true);
			if (showDate) dateText.Draw();
			if (showDay) dayText.Draw();
			if (showTime) timeText.Draw();
			if (showSpeed) speedText.Draw();
			if (showEngine) engineHealthText.Draw();
			if (showHealth) healthText.Draw();
			if (showArmor) armorText.Draw();
			if (showMoney) moneyText.Draw();
		}
	}


}
